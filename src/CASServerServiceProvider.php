<?php
/**
 * Created by PhpStorm.
 * User: chenyihong
 * Date: 16/8/13
 * Time: 17:49
 */

namespace SpringsCS\SSO;

use Illuminate\Support\ServiceProvider;
use SpringsCS\SSO\Console\Commands\LogoutExpiredSessions;

/**
 * Class CASServerServiceProvider
 * @package SpringsCS\SSO
 */
class CASServerServiceProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        // TODO: Implement register() method.
    }

    /**
     * @inheritdoc
     */
    public function boot()
    {
        require __DIR__.'/Utils/helpers.php';

        if (! $this->app->routesAreCached()) {
            require __DIR__.'/Http/routes.php';
        }

        $this->loadTranslationsFrom(__DIR__.'/../resources/lang/', 'sso');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations/');
        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'sso');

        $this->publishes([
            __DIR__ . '/../config/cas.php' => config_path('cas.php'),
            __DIR__ . '/../config/saml.php' => config_path('saml.php'),
        ], 'sso-config');

        $this->publishes([
            __DIR__.'/../resources/views/' => resource_path('views/vendor/sso'),
//            __DIR__ . '/../resources/views/saml.blade.php' => resource_path('views/vendor/saml.blade.php')
        ], 'sso-views');

        $this->publishes([
            __DIR__.'/../resources/lang/' => resource_path('lang/vendor/sso'),
        ], 'sso-lang');

        if ($this->app->runningInConsole()) {
            $this->commands([
                LogoutExpiredSessions::class,
            ]);
        }
    }
}
