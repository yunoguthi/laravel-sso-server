<?php

namespace SpringsCS\SSO\Events;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use SpringsCS\SSO\Contracts\Models\UserModel;

class SSOUserLoginEvent extends Event
{
    use SerializesModels;

    /**
     * @var Request
     */
    protected $sessionId;
    /**
     * @var UserModel
     */
    protected $user;

    /**
     * SSOUserLoginEvent constructor.
     * @param Request   $request
     * @param UserModel $user
     */
    public function __construct($sessionId, UserModel $user)
    {
        $this->sessionId = $sessionId;
        $this->user    = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    /**
     * @return Request
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @return UserModel
     */
    public function getUser()
    {
        return $this->user;
    }
}
