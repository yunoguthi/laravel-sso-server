<?php
/**
 * Created by PhpStorm.
 * User: leo108
 * Date: 16/9/18
 * Time: 11:28
 */

namespace SpringsCS\SSO\Events;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use SpringsCS\SSO\Contracts\Models\UserModel;

class SSOUserLogoutEvent extends Event
{
    use SerializesModels;
    /**
     * @var Request
     */
    protected $sessionId;
    /**
     * @var UserModel
     */
    protected $user;

    /**
     * SSOUserLoginEvent constructor.
     * @param Request   $request
     * @param UserModel $user
     */
    public function __construct(UserModel $user, $sessionId = null)
    {
        $this->sessionId = $sessionId ?? session()->getId();
        $this->user    = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    /**
     * @return Request
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @return UserModel
     */
    public function getUser()
    {
        return $this->user;
    }
}
