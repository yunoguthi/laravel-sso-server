<?php
/**
 * Created by PhpStorm.
 * User: chinanumbahtwo
 * Date: 3/14/18
 * Time: 12:57 PM
 */

namespace SpringsCS\SSO\Repositories\Saml;


use SpringsCS\SSO\Models\Saml\ServiceProvider;
use SpringsCS\SSO\Requests\Saml\EntityDescriptor;

class ServiceProviderRepository
{
    protected $serviceProvider;

    public function __construct(EntityDescriptor $sp = null)
    {
        $this->serviceProvider = $sp;
    }

    public function insertSP($request = null)
    {
        $entityId = $this->serviceProvider->getEntityId();

        $attributes = [
            'entity_id' => $entityId,
            'sso_service_location' => $this->serviceProvider->getSingleSignOnLocation(),
            'slo_service_location' => $this->serviceProvider->getSingleLogoutLocation()
        ];

        // Extract extra data from request
        if ($request && $request->has('metadataUrl') && $request->has('sloSupport')) {
            $attributes = array_merge($attributes, [
                $request->get('metadataUrl'),
                $request->get('sloSupport'),
            ]);
        }

        ServiceProvider::updateOrCreate(
            [
                'entity_id' => $entityId
            ],
            $attributes
        );
    }

    public function getSP($entityId)
    {
        return ServiceProvider::where('entity_id', $entityId)->first();
    }
}