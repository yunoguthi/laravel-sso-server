<?php
/**
 * Created by PhpStorm.
 * User: chinanumbahtwo
 * Date: 3/19/18
 * Time: 9:58 AM
 */

namespace SpringsCS\SSO\Repositories\Saml;

use App\User;
use Carbon\Carbon;
use LightSaml\Model\Protocol\AuthnRequest;
use SpringsCS\SSO\Models\Saml\Session;
use SpringsCS\SSO\Requests\Saml\LogoutRequest;

class SessionRepository
{
    public function create(User $user, AuthnRequest $authnRequest)
    {
        $entityId = $authnRequest->getIssuer()->getValue();

        $serviceProviderRepository = new ServiceProviderRepository();
        $sp = $serviceProviderRepository->getSP($entityId);
        if (is_null($sp)){
            throw new \Exception('Service Provider "' .$entityId. '" not registered with IDP');
        };

        $serviceId = $sp->id;

        Session::create([
            'service_id' => $serviceId,
            'user_id' => $user->id,
            'session_id' => session()->getId(),
            'expire_at' => new Carbon(sprintf('+%dsec', config('saml.session.expireAt', 7200))),
        ]);
    }

    public function getSessions($userId, $sessionId)
    {
        $sessionsToDelete = Session::with('serviceProvider')
            ->where('user_id', $userId)
            ->where('session_id', $sessionId)->get();

        return $sessionsToDelete;
    }

    public static function getExpiredSessions()
    {
        return Session::where('expire_at', '<=', Carbon::now())->get();
    }
}