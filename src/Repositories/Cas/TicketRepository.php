<?php
/**
 * Created by PhpStorm.
 * User: leo108
 * Date: 16/9/17
 * Time: 20:19
 */

namespace SpringsCS\SSO\Repositories\Cas;

use Carbon\Carbon;
use SpringsCS\SSO\Contracts\Models\UserModel;
use SpringsCS\SSO\Exceptions\CAS\CasException;
use SpringsCS\SSO\Models\Cas\Ticket;
use SpringsCS\SSO\Services\TicketGenerator;
use SpringsCS\SSO\Requests\Saml\LogoutRequest;

class TicketRepository
{
    /**
     * @var Ticket
     */
    protected $ticket;

    /**
     * @var ServiceRepository
     */
    protected $serviceRepository;

    /**
     * @var TicketGenerator
     */
    protected $ticketGenerator;

    /**
     * TicketRepository constructor.
     * @param Ticket            $ticket
     * @param ServiceRepository $serviceRepository
     * @param TicketGenerator   $ticketGenerator
     */
    public function __construct(Ticket $ticket, ServiceRepository $serviceRepository, TicketGenerator $ticketGenerator)
    {
        $this->ticket            = $ticket;
        $this->serviceRepository = $serviceRepository;
        $this->ticketGenerator   = $ticketGenerator;
    }

    /**
     * @param UserModel $user
     * @param string    $serviceUrl
     * @param string    $sessionId
     * @param array     $proxies
     * @throws CasException
     * @return Ticket
     */
    public function applyTicket(UserModel $user, $serviceUrl, $sessionId, $proxies = [])
    {
        $service = $this->serviceRepository->getServiceByUrl($serviceUrl);
        if (!$service) {
            throw new CasException(CasException::INVALID_SERVICE);
        }
        $ticket = $this->getAvailableTicket(config('cas.ticket_len', 32), empty($proxies) ? 'ST-' : 'PT-');
        if ($ticket === false) {
            throw new CasException(CasException::INTERNAL_ERROR, 'apply ticket failed');
        }
        $existingTicket = $this->ticket->where('session_id', $sessionId)->where('service_url', $serviceUrl)
            ->first();

        if($existingTicket)
            return $existingTicket;

        $record = $this->ticket->newInstance(
            [
                'ticket'      => $ticket,
                'session_id'  => $sessionId,
                'expire_at'   => new Carbon(sprintf('+%dsec', config('cas.ticket_validate_expire', 300))),
                'created_at'  => new Carbon(),
                'service_url' => $serviceUrl,
                'proxies'     => $proxies,
            ]
        );
        $record->user()->associate($user->getEloquentModel());
        $record->service()->associate($service);
        $record->save();

        return $record;
    }

    /**
     * @param string $ticket
     * @param bool   $checkExpired
     * @return null|Ticket
     */
    public function getByTicket($ticket, $checkExpired = true)
    {
        $record = $this->ticket->where('ticket', $ticket)->first();
        if (!$record) {
            return null;
        }

        return ($checkExpired && $record->isExpired()) ? null : $record;
    }

    /**
     * @param Ticket $ticket
     * @return bool|null
     */
    public function invalidTicket(Ticket $ticket)
    {
        return $ticket->delete();
    }

    public function getTickets($userId, $sessionId)
    {

        $ticketsToDelete = Ticket::with('service.hosts')->where('user_id', $userId)->where('session_id', $sessionId)->get();

        return $ticketsToDelete;
    }

    public function ticketValidated(Ticket $ticket)
    {
        $ticket->validated_at = new Carbon();
        $ticket->expire_at = new Carbon(sprintf('+%dsec', config('cas.ticket_expire', 7200)));
        $ticket->save();
    }

    /**
     * @param integer $totalLength
     * @param string  $prefix
     * @return string|false
     */
    protected function getAvailableTicket($totalLength, $prefix)
    {
        return $this->ticketGenerator->generate(
            $totalLength,
            $prefix,
            function ($ticket) {
                return is_null($this->getByTicket($ticket, false));
            },
            10
        );
    }

    public static function getExpiredTickets()
    {
        return Ticket::where('expire_at', '<=', Carbon::now())->get();
    }
}
