<?php
/**
 * Created by PhpStorm.
 * User: chinanumbahtwo
 * Date: 3/13/18
 * Time: 9:21 AM
 */

namespace SpringsCS\SSO\Listeners;


use SpringsCS\SSO\Events\SSOUserLogoutEvent;
use SpringsCS\SSO\Repositories\Cas\TicketRepository;
use SpringsCS\SSO\Repositories\Saml\SessionRepository;
use SpringsCS\SSO\Requests\Saml\LogoutRequest;

class SSOUserLogoutListener
{
    protected $ticketRepository;
    protected $sessionRepository;

    public function __construct(TicketRepository $ticketRepository, SessionRepository $sessionRepository)
    {
        $this->ticketRepository = $ticketRepository;
        $this->sessionRepository = $sessionRepository;
    }

    public function handle(SSOUserLogoutEvent $event)
    {
        $user = $event->getUser();
        $sessionId = $event->getSessionId();

        // Delete CAS tickets
        $ticketsToDelete = $this->ticketRepository->getTickets($user->id, $sessionId);

        foreach ($ticketsToDelete as $ticketToDelete) {
            $logoutRequest = LogoutRequest::create($ticketToDelete->ticket,
                $ticketToDelete->service_url);
            LogoutRequest::send($logoutRequest, 'logoutRequest');
            $ticketToDelete->delete();
        }

        // Delete SAML sessions
        $sessions = $this->sessionRepository->getSessions($user->id, $sessionId);

        foreach ($sessions as $session) {
            optional($session->serviceProvider->slo_service_location, function () use ($session) {
                $logoutRequest = LogoutRequest::create(
                    $session->session_id,
                    $session->serviceProvider->slo_service_location
                );

                LogoutRequest::send($logoutRequest, 'SAMLRequest');
                $session->delete();
            });
        }
    }
}