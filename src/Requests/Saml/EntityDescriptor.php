<?php
/**
 * Created by PhpStorm.
 * User: chinanumbahtwo
 * Date: 3/14/18
 * Time: 1:19 PM
 */

namespace SpringsCS\SSO\Requests\Saml;

use LightSaml\Model\Context\SerializationContext;
use LightSaml\Model\Metadata\EntityDescriptor as SamlEntityDescriptor;
use LightSaml\SamlConstants;
use LightSaml\Builder\EntityDescriptor\SimpleEntityDescriptorBuilder;

class EntityDescriptor
{
    protected $entityDescriptor;

    public function __construct($metadataUrl = null)
    {
        if(!is_null($metadataUrl))
            $this->entityDescriptor = SamlEntityDescriptor::load($metadataUrl);
    }

    public function getEntityId()
    {
        return $this->entityDescriptor->getEntityID();
    }

    public function getSingleSignOnLocation()
    {
        $ssoLocation = $this->entityDescriptor->getFirstSpSsoDescriptor()
            ->getFirstAssertionConsumerService(SamlConstants::BINDING_SAML2_HTTP_POST);
       try {
           return $ssoLocation->getLocation();
       }
       catch(Symfony\Component\Debug\Exception\FatalThrowableError $e){
           throw new \Exception('Assertion Consumer Service Protocol not supported (POST only supported');
       }
    }

    public function getSingleLogoutLocation()
    {
        return $this->entityDescriptor->getFirstSpSsoDescriptor()
            ->getFirstSingleLogoutService()->getLocation();
    }

    public function buildIdpMetadata($entityId, $acsLocation,
                                     $ssoLocation, $certificate)
    {
        $entityDescriptorBuilder = new \LightSaml\Builder\EntityDescriptor\SimpleEntityDescriptorBuilder(
            $entityId,
            $acsLocation,
            $ssoLocation,
            \LightSaml\Credential\X509Certificate::fromFile($certificate)
        );
        $entityDescriptor = $entityDescriptorBuilder->get();
        $serializationContext = new SerializationContext();
        $entityDescriptor->serialize($serializationContext->getDocument(), $serializationContext);
        return $serializationContext->getDocument()->saveXml();
    }
}