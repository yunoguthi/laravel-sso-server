<?php

namespace SpringsCS\SSO\Requests\Saml;

use Exception;
use LightSaml\ClaimTypes;
use LightSaml\Credential\KeyHelper;
use LightSaml\Credential\X509Certificate;
use LightSaml\Helper;
use LightSaml\Model\Assertion\Assertion;
use LightSaml\Model\Assertion\Attribute;
use LightSaml\Model\Assertion\AttributeStatement;
use LightSaml\Model\Assertion\AudienceRestriction;
use LightSaml\Model\Assertion\AuthnContext;
use LightSaml\Model\Assertion\AuthnStatement;
use LightSaml\Model\Assertion\Conditions;
use LightSaml\Model\Assertion\Issuer;
use LightSaml\Model\Assertion\NameID;
use LightSaml\Model\Assertion\Subject;
use LightSaml\Model\Assertion\SubjectConfirmation;
use LightSaml\Model\Assertion\SubjectConfirmationData;
use LightSaml\Model\Protocol\AuthnRequest;
use LightSaml\Model\Protocol\Response as SamlResponse;
use LightSaml\Model\Protocol\Status;
use LightSaml\Model\Protocol\StatusCode;
use LightSaml\Model\XmlDSig\SignatureWriter;
use LightSaml\SamlConstants;
use SpringsCS\SSO\Contracts\Models\UserModel;
use SpringsCS\SSO\Models\Saml\ServiceProvider;
use SpringsCS\SSO\Repositories\Saml\ServiceProviderRepository;

class Response
{
    /**
     * @param AuthnRequest $request
     * @param UserModel $user
     * @param $relayState
     * @return SamlResponse
     * @throws Exception
     */
    public static function build(AuthnRequest $request, UserModel $user, $relayState)
    {
        $destination = $request->getAssertionConsumerServiceURL();
        $issuer = config('saml.entityId');
        $cert = storage_path(config('saml.x509CertPath'));
        $key = storage_path(config('saml.privateKeyPath'));

        $certificate = X509Certificate::fromFile($cert);
        $privateKey = KeyHelper::createPrivateKey($key, '', true);

        $response = new SamlResponse();
        $response
            ->addAssertion($assertion = new Assertion())
            ->setID(Helper::generateID())
            ->setIssueInstant(new \DateTime())
            ->setDestination($destination)
            ->setIssuer(new Issuer($issuer))
            ->setStatus(new Status(new StatusCode('urn:oasis:names:tc:SAML:2.0:status:Success')))
            ->setSignature(new SignatureWriter($certificate, $privateKey));

        if ($relayState)
            $response->setRelayState($relayState);

        // load SP to determine which attribute to use
        $serviceProviderRepository = new ServiceProviderRepository();
        $remoteEntity = $request->getIssuer()->getValue();
        /** @var ServiceProvider $sp */
        $sp = $serviceProviderRepository->getSP($remoteEntity);
        if (is_null($sp)){
            throw new \Exception('Service Provider "' .$remoteEntity. '" not registered with IDP');
        };

        if (! $user) {
            throw new Exception('Unable to get user');
        }

        $name = $user->name;
        $email = $user->email;

        $nameIdField = $sp->getAttribute('nameid_field') ?? 'email';
        $nameID = new NameID(
            $user->getAttribute($nameIdField),
            $nameIdField === 'email' ?
                SamlConstants::NAME_ID_FORMAT_EMAIL : SamlConstants::NAME_ID_FORMAT_TRANSIENT
        );

        $assertion
            ->setId(Helper::generateID())
            ->setIssueInstant(new \DateTime())
            ->setIssuer(new Issuer($issuer))
            ->setSubject(
                (new Subject())
                    ->setNameID($nameID)
                    ->addSubjectConfirmation(
                        (new SubjectConfirmation())
                            ->setMethod(SamlConstants::CONFIRMATION_METHOD_BEARER)
                            ->setSubjectConfirmationData(
                                (new SubjectConfirmationData())
                                    ->setInResponseTo($request->getId())
                                    ->setNotOnOrAfter(new \DateTime('+1 MINUTE'))
                                    ->setRecipient($request->getAssertionConsumerServiceURL())
                            )
                    )
            )
            ->setConditions(
                (new Conditions())
                    ->setNotBefore(new \DateTime())
                    ->setNotOnOrAfter(new \DateTime('+1 MINUTE'))
                    ->addItem(
                        new AudienceRestriction([$request->getAssertionConsumerServiceURL()])
                    )
            )
            ->addItem(
                (new AttributeStatement())
                    ->addAttribute(new Attribute(
                        ClaimTypes::EMAIL_ADDRESS,
                        $email
                    ))
                    ->addAttribute(new Attribute(
                        ClaimTypes::COMMON_NAME,
                        $name
                    ))
                    ->addAttribute(new Attribute(
                        ClaimTypes::GIVEN_NAME,
                        $user->firstname
                    ))
                    ->addAttribute(new Attribute(
                        ClaimTypes::SURNAME,
                        $user->lastname
                    ))
            )
            ->addItem(
                (new AuthnStatement())
                    ->setAuthnInstant(new \DateTime('-10 MINUTE'))
                    ->setSessionIndex(session()->getId())
                    ->setAuthnContext(
                        (new AuthnContext())
                            ->setAuthnContextClassRef(
                                SamlConstants::AUTHN_CONTEXT_PASSWORD_PROTECTED_TRANSPORT
                            )
                    )
            )
        ;
        return $response;
    }
}