<?php
/**
 * Created by PhpStorm.
 * User: chinanumbahtwo
 * Date: 3/20/18
 * Time: 2:55 PM
 */

namespace SpringsCS\SSO\Requests\Saml;

use GuzzleHttp\Exception\GuzzleException;
use LightSaml\Model\Assertion\Issuer;
use LightSaml\Model\Assertion\NameID;
use LightSaml\Model\Protocol\LogoutRequest as SamlLogoutRequest;
use LightSaml\Model\Context\SerializationContext;
use LightSaml\Helper;
use LightSaml\Binding\BindingFactory;
use LightSaml\SamlConstants;
use LightSaml\Context\Profile\MessageContext;
use GuzzleHttp\Client;

class LogoutRequest
{
    public static function create($sessionIndex, $destination)
    {
        $request = new SamlLogoutRequest();
        $request->setID(Helper::generateID());
        $request->setIssueInstant(new \DateTime());
        $request->setNameId(new NameID($sessionIndex));
        $request->setSessionIndex($sessionIndex);
        $request->setDestination($destination);
        $request->setIssuer(new Issuer(config('saml.entityId')));

        return $request;
    }

    public static function send(SamlLogoutRequest $request, $requestName)
    {
        $client = new Client(['verify' => false]);
        $destination = $request->getDestination();

        $serializationContext = new SerializationContext();

        $request->serialize($serializationContext->getDocument(), $serializationContext);

        $xml = $serializationContext->getDocument()->saveXML();

        $method = 'POST';
        if ($requestName == 'SAMLRequest') {
            $xml = base64_encode($xml);
            $method = 'GET';
        }
        $xml = urlencode($xml);

        $url = $destination . '?'.$requestName.'='.$xml;
        try {
            $response = $client->request($method, $url);
        } catch (GuzzleException $e) {
            // Do nothing here
        }
    }
}