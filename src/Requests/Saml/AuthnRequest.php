<?php
/**
 * Created by PhpStorm.
 * User: chinanumbahtwo
 * Date: 3/15/18
 * Time: 1:35 PM
 */

namespace SpringsCS\SSO\Requests\Saml;

use LightSaml\Model\Context\DeserializationContext;
use LightSaml\Model\Protocol\AuthnRequest as SamlAuthnRequest;

class AuthnRequest
{
    protected $authnRequest;

    public function __construct($xml = null)
    {
        $deserializationContext = new DeserializationContext() ;
        $deserializationContext->getDocument()->loadXML($xml);

        $this->authnRequest = new SamlAuthnRequest();
        $this->authnRequest->deserialize($deserializationContext->getDocument()->firstChild, $deserializationContext);
    }

    public function get()
    {
        return $this->authnRequest;
    }
}