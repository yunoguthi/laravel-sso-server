<?php
/**
 * Created by PhpStorm.
 * User: chinawasalie
 * Date: 3/26/18
 * Time: 9:03 AM
 */

namespace SpringsCS\SSO\Console\Commands;

use Illuminate\Console\Command;
use SpringsCS\SSO\Repositories\Cas\TicketRepository;
use SpringsCS\SSO\Repositories\Saml\SessionRepository;
use App\User;
use SpringsCS\SSO\Events\SSOUserLogoutEvent;


class LogoutExpiredSessions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sso:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks both SAML and CAS sessions / tickets. Removes expired sessions / tickets, and 
                              sends a single LogoutRequest to expired SP\'s';

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        // Check SAML sessions
        $expiredSessions = SessionRepository::getExpiredSessions();
        foreach($expiredSessions as $expiredSession){
            $user = User::where('id', $expiredSession->user_id)->first();
            event(new SSOUserLogoutEvent(
                $user,
                $expiredSession->session_id
            ));
            session()->getHandler()->destroy($expiredSession->session_id);
        }

        // Check CAS tickets
        $expiredTickets = TicketRepository::getExpiredTickets();
        foreach($expiredTickets as $expiredTicket){
            $user = User::where('id', $expiredTicket->user_id)->first();
            event(new SSOUserLogoutEvent(
                $user,
                $expiredTicket->session_id
            ));
            session()->getHandler()->destroy($expiredTicket->session_id);
        }
    }
}