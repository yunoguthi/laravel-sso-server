<?php

namespace SpringsCS\SSO\Http\Controllers;

use Illuminate\Http\Request;
use LightSaml\Binding\BindingFactory;
use LightSaml\Context\Profile\MessageContext;
use LightSaml\Model\Assertion\Issuer;
use LightSaml\Model\Protocol\AuthnRequest as RawAuthnRequest;
use LightSaml\SamlConstants;
use SpringsCS\SSO\Models\Saml\ServiceProvider as SamlServiceProvider;
use SpringsCS\SSO\Repositories\Saml\ServiceProviderRepository;
use SpringsCS\SSO\Repositories\Saml\SessionRepository;
use SpringsCS\SSO\Requests\Saml\AuthnRequest;
use SpringsCS\SSO\Requests\Saml\EntityDescriptor;
use SpringsCS\SSO\Contracts\Interactions\UserLogin;
use SpringsCS\SSO\Contracts\Models\UserModel;
use SpringsCS\SSO\Events\SSOUserLoginEvent;
use SpringsCS\SSO\Requests\Saml\Response;
use SpringsCS\SSO\Events\SSOUserLogoutEvent;
use LightSaml\Context\Profile\Helper\MessageContextHelper;


class SamlController extends Controller
{
    protected $loginInteraction;

    protected $sessionRepository;

    public function __construct(UserLogin $loginInteraction, SessionRepository $sessionRepository)
    {
        $this->loginInteraction = $loginInteraction;
        $this->sessionRepository = $sessionRepository;
    }

    public function registerSp(Request $request)
    {
        $metadataUrl = $request->get('metadataUrl');

        $entityDescriptor = new EntityDescriptor($metadataUrl);

        $spRepository = new ServiceProviderRepository($entityDescriptor);

        $spRepository->insertSP($request);
    }

    public function serveMetadata(Request $request)
    {
        $entityDescriptor = new EntityDescriptor();
        return response($entityDescriptor->buildIdpMetadata(
            config('saml.entityId'),
            null,
            config('saml.loginEndpoint'),
            storage_path(config('saml.x509CertPath'))
        ))->header('Content-Type', 'application/xml');

    }

    public function showLogin(Request $request)
    {
        $user = $this->loginInteraction->getCurrentUser($request);
        if ($user)
            return $this->authenticated($request, $user);
    }

    public function login(Request $request)
    {
        if (! isset($request['SAMLRequest'])) {
            return redirect('/');
        }

        $user = $this->loginInteraction->getCurrentUser($request);
        if (is_null($user)) {
            return $this->loginInteraction->showAuthenticateFailed($request);
        }

        $response = $this->authenticated($request, $user);

        return $this->sendSAMLResponse($response);
    }

    /**
     * @param Request $request
     * @param UserModel $user
     * @return \LightSaml\Model\Protocol\Response
     * @throws \Exception
     */
    public function authenticated(Request $request, UserModel $user)
    {
        event(new SSOUserLoginEvent($request, $user));

        $saml = $request->get('SAMLRequest');
        $decoded = base64_decode($saml);
        try {
            $xml = gzinflate($decoded);
        } catch (\Exception $e) {
            $xml = $decoded;
        }

        $relayState = $request->has('RelayState') ? $request->get('RelayState') : null;

        $authnRequest = new AuthnRequest($xml);
        $response = Response::build($authnRequest->get(), $user, $relayState);
//        $samlResponse = new Response($authnRequest);
//        $response = $samlResponse->build($authnRequest, $user, $relayState);

        $this->sessionRepository->create($user, $authnRequest->get());

        return $response;
    }

    /**
     * @param Request $request
     * @param SamlServiceProvider $sp
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function idpInitiated(Request $request, SamlServiceProvider $sp)
    {
        $user = $this->loginInteraction->getCurrentUser($request);
        if (! $user)
            return $this->loginInteraction->showAuthenticateFailed($request);

        // create a new request for the SP
        $samlRequest = new RawAuthnRequest();
        $samlRequest->setAssertionConsumerServiceURL($sp->sso_service_location);
        $samlRequest->setID('LS3-'.uniqid());
        $issuer = new Issuer();
        $issuer->setValue($sp->entity_id);
//        $issuer->setValue(config('saml.entityId'));
        $samlRequest->setIssuer($issuer);

        $response = Response::build($samlRequest, $user, null);
        $this->sessionRepository->create($user, $samlRequest);

        return $this->sendSAMLResponse($response);
//        $response = $samlResponse->build($request, $user, null);
    }


    public function logout(Request $request)
    {
        return $this->loginInteraction->showLoggedOut($request);
    }

    protected function sendSAMLResponse($response)
    {
        $messageContext = new MessageContext();
        $messageContext->setMessage($response)->asResponse();
        $message = MessageContextHelper::asSamlMessage($messageContext);
        $serializationContext = $messageContext->getSerializationContext();
        $message->serialize($serializationContext->getDocument(), $serializationContext);
        $msgStr = $serializationContext->getDocument()->saveXML();

        $data = array('SAMLResponse' => base64_encode($msgStr));
        if ($message->getRelayState()) {
            $data['RelayState'] = $message->getRelayState();
        }

        $destination = $message->getDestination();
        return view('sso::saml', compact('data', 'destination'));
    }


}