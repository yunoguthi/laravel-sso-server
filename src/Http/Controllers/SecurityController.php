<?php
/**
 * Created by PhpStorm.
 * User: chenyihong
 * Date: 16/8/1
 * Time: 14:50
 */

namespace SpringsCS\SSO\Http\Controllers;

use SpringsCS\SSO\Contracts\Interactions\UserLogin;
use SpringsCS\SSO\Contracts\Models\UserModel;
use SpringsCS\SSO\Events\SSOUserLoginEvent;
use SpringsCS\SSO\Events\SSOUserLogoutEvent;
use SpringsCS\SSO\Exceptions\CAS\CasException;
use Illuminate\Http\Request;
use SpringsCS\SSO\Repositories\Cas\PGTicketRepository;
use SpringsCS\SSO\Repositories\Cas\ServiceRepository;
use SpringsCS\SSO\Repositories\Cas\TicketRepository;

class SecurityController extends Controller
{
    /**
     * @var ServiceRepository
     */
    protected $serviceRepository;

    /**
     * @var TicketRepository
     */
    protected $ticketRepository;

    /**
     * @var PGTicketRepository
     */
    protected $pgTicketRepository;
    /**
     * @var UserLogin
     */
    protected $loginInteraction;

    /**
     * SecurityController constructor.
     * @param ServiceRepository  $serviceRepository
     * @param TicketRepository   $ticketRepository
     * @param PGTicketRepository $pgTicketRepository
     * @param UserLogin          $loginInteraction
     */
    public function __construct(
        ServiceRepository $serviceRepository,
        TicketRepository $ticketRepository,
        PGTicketRepository $pgTicketRepository,
        UserLogin $loginInteraction
    ) {
        $this->serviceRepository  = $serviceRepository;
        $this->ticketRepository   = $ticketRepository;
        $this->loginInteraction   = $loginInteraction;
        $this->pgTicketRepository = $pgTicketRepository;
    }

    public function showLogin(Request $request)
    {
        $service = $request->get('service', '');
        if (! empty($service)) {
            // If unable to find service, then throw and error
            if (! $this->serviceRepository->isUrlValid($service)) {
                throw new CasException(CasException::INVALID_SERVICE);
            }
        }

        $user = $this->loginInteraction->getCurrentUser($request);

        // Is user already logged in?
        if ($user) {
            return $this->authenticated($request, $user);
        } else {
            return $this->loginInteraction->showLoginPage($request, [], 'cas');
        }
    }

    public function login(Request $request)
    {
        $user = $this->loginInteraction->login($request);
        if (is_null($user)) {
            return $this->loginInteraction->showAuthenticateFailed($request);
        }

        return $this->authenticated($request, $user);
    }

    public function authenticated(Request $request, UserModel $user)
    {
        event(new SSOUserLoginEvent($request, $user));
        $serviceUrl = $request->get('service', '');
        $sessionId = session()->getId();

        // Redirect to service or home if there was no service
        if (! empty($serviceUrl)) {
            $query = parse_url($serviceUrl, PHP_URL_QUERY);
            $ticket = $this->ticketRepository->applyTicket($user, $serviceUrl, $sessionId);
            $finalUrl = $serviceUrl.($query ? '&' : '?').'ticket='.$ticket->ticket;

            return redirect($finalUrl);
        }

        return $this->loginInteraction->redirectToHome();
    }

    public function logout(Request $request)
    {
        /*$user = $this->loginInteraction->getCurrentUser($request);
        if ($user) {
            event(new SSOUserLogoutEvent($user, session()->getId()));
            $this->loginInteraction->logout($request);
            $this->pgTicketRepository->invalidTicketByUser($user);
        }*/
        /*$service = $request->get('service');
        if ($service && $this->serviceRepository->isUrlValid($service)) {
            return redirect($service);
        }*/

        return $this->loginInteraction->showLoggedOut($request);
    }
}
