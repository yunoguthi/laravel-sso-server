<?php
/**
 * Created by PhpStorm.
 * User: chinawasalie
 * Date: 5/9/18
 * Time: 3:05 PM
 */

namespace SpringsCS\SSO\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as ParentAuthenticate;
use Closure;

class Authenticate extends ParentAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        if ($request->has('RelayState')) {
            session(['RelayState' => $request->get('RelayState')]);
        }
//        \Log::info(session('RelayState'));

        return parent::handle($request, $next, $guards);
    }
}