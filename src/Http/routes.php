<?php

$casOptions = [
    'prefix'    => config('cas.router.prefix'),
    'namespace' => '\SpringsCS\SSO\Http\Controllers',
];

if (config('cas.middleware.common')) {
    $casOptions['middleware'] = config('cas.middleware.common');
}

Route::group(
    $casOptions,
    function () {
        $auth = config('cas.middleware.auth');
        $p    = config('cas.router.name_prefix');
        Route::get('login', 'SecurityController@showLogin')->name($p.'login.get'); //->middleware($auth)
        Route::post('login', 'SecurityController@login')->middleware($auth)->name($p.'login.post');

        Route::get('logout', 'SecurityController@logout')->name($p.'logout')->middleware($auth);

        Route::any('validate', 'ValidateController@v1ValidateAction')->name($p.'v1.validate');
        Route::any('serviceValidate', 'ValidateController@v2ServiceValidateAction')->name($p.'v2.validate.service');
        Route::any('proxyValidate', 'ValidateController@v2ProxyValidateAction')->name($p.'v2.validate.proxy');
        Route::any('proxy', 'ValidateController@proxyAction')->name($p.'proxy');

        Route::any('p3/serviceValidate', 'ValidateController@v3ServiceValidateAction')->name($p.'v3.validate.service');
        Route::any('p3/proxyValidate', 'ValidateController@v3ProxyValidateAction')->name($p.'v3.validate.proxy');
    }
);

$samlOptions = [
    'prefix'    => config('saml.router.prefix'),
    'namespace' => '\SpringsCS\SSO\Http\Controllers',
];

if (config('cas.middleware.common')) {
    $samlOptions['middleware'] = config('saml.middleware.common');
}

Route::group(
    $samlOptions,
    function(){
        $auth = config('saml.middleware.auth');
        $p = config('saml.router.name_prefix');
        Route::post('register-sp', 'SamlController@registerSp')->name($p.'register');
        Route::get('metadata', 'SamlController@serveMetadata')->name($p.'metadata');
        Route::get('login', 'SamlController@login')->name($p.'login')->middleware($auth);
        Route::post('login', 'SamlController@login')->middleware($auth);
        Route::get('logout', 'SamlController@logout')->name($p.'logout');

        Route::get('idp/{sp}', 'SamlController@idpInitiated')->middleware($auth);
    }
);
