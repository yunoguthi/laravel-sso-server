<?php
/**
 * Created by PhpStorm.
 * User: chinanumbahtwo
 * Date: 3/19/18
 * Time: 10:01 AM
 */

namespace SpringsCS\SSO\Models\Saml;


use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    public $table = 'saml_sessions';
    public $timestamps = true;

    protected $guarded = ['id'];

    public function serviceProvider()
    {
        return $this->hasOne(ServiceProvider::class, 'id', 'service_id');
    }
}