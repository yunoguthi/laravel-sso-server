<?php
/**
 * Created by PhpStorm.
 * User: chinanumbahtwo
 * Date: 3/14/18
 * Time: 1:00 PM
 */

namespace SpringsCS\SSO\Models\Saml;

use Illuminate\Database\Eloquent\Model;


class ServiceProvider extends Model
{
    protected $table = 'saml_service_providers';
    public $timestamps = false;

    protected $guarded = ['id'];
}