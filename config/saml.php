<?php

return [
    'entityId' => env('SAML_ENTITY_ID', ''),
    'loginEndpoint' => env('SAML_LOGIN_URL', ''),
    'logoutEndpoint' => env('SAML_LOGOUT_URL', ''),
    'nameIdFormat' => env('SAML_NAME_FORMAT', ''),
    // Path is relative to the storage directory
    'x509CertPath' => env('SAML_X509_CERT_PATH', 'app/certs/samlIdp'),
    'privateKeyPath' => env('SAML_PRIVATE_KEY_PATH', 'app/keys/samlIdp'),

    'session' => [
        'expireAt' => env('SAML_SESSION_TIMEOUT', 7200),
    ],

    'router' => [
        'prefix' => 'saml',
        'name_prefix' => 'saml.'
    ],
    'middleware' => [
        'common' => 'web',
        'auth'   => 'auth.saml',
    ],
];