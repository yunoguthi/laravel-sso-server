<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>POST data</title>
</head>
<body onload="submitForm();">

<noscript>
    <p><strong>Note:</strong> Since your browser does not support JavaScript, you must press the button below once to proceed.</p>
</noscript>

<form method="post" action="{{$destination}}" id="form">
    <input type="submit" style="display:none;" />
    <input type="hidden" name="SAMLResponse" value="{{$data['SAMLResponse']}}"/>
    @if (key_exists('RelayState', $data))
        <input type="hidden" name="RelayState" id="RelayState" value="{{$data['RelayState']}}"/>
    @endif

    <noscript>
        <input type="submit" value="Submit" />
    </noscript>

</form>
</body>


<script type="text/javascript" src="//ssl.gstatic.com/accounts/chrome/users-1.0.js"></script>
<script>
    function submitForm() {
        if ("{{ session()->has('isChromeOS') }}") {
            var form = document.getElementById('form')
            google.principal.complete(
                {
                    token: form.RelayState.value
                },
                form.submit.bind(form)
            )
        }
        else {
            document.getElementsByTagName('input')[0].click()
        }
    }
</script>
</html>