<?php

return [
    'invalid_request' => 'Unknown or bad request received.',
    'invalid_ticket' => 'Ticket is no longer valid and may be expired. Please try again.',
    'invalid_service' => 'There is no service with the specified URL.',
    'internal_error' => 'An internal server error occurred.',
    'unauthorized_service_proxy' => 'Proxy tickets are not enabled for this service.',
];