<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('cas_tickets')) {
            Schema::create('cas_tickets', function (Blueprint $table) {
                $table->charset = 'utf8';
                $table->collation = 'utf8_general_ci';
                $table->increments('id');
                $table->string('ticket', 32)->unique();
                $table->string('service_url', 1024);
                $table->integer('service_id')->unsigned();
                $table->string('session_id');
                $table->integer('user_id')->unsigned();
                $table->timestamp('validated_at')->nullable();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('expire_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cas_tickets');
    }
}
