<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToSamlSpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('saml_service_providers', function (Blueprint $table) {
            $table->text('name')->after('id');
            $table->boolean('single_logout_enabled')->before('slo_service_location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saml_service_providers', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('single_logout_enabled');
        });
    }
}
